﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    public static GameController instance;

    public Player player;

    private Vector3 origin;

    private int score;
    private int lives;
    private int highscore;

    public Text scoreText;
    public Text livesText;
    public Text highscoreText;


    private void Awake()
    {
        // Setup the singleton
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start () {
        highscore = PlayerPrefs.GetInt("highscore", 0);
        origin = player.transform.position;
        
        BeginGame();
	}

    void BeginGame()
    {
        score = 0;
        lives = 3;

        scoreText.text = "SCORE: " + score;
        highscoreText.text = "HISCORE: " + highscore;
        livesText.text = "LIVES: " + lives;


    }

    public void DecrementLives()
    {
        lives--;
        livesText.text = "LIVES: " + lives;


        // Has player run out of lives?
        if (lives < 1)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }

        Respawn();
    }
   
    void Respawn()
    {
        player.transform.position = origin;
    }
   

    public void IncrementScore()
    {
        score++;
        scoreText.text = "SCORE:" + score;

        if (score > highscore)
        {
            highscore = score;
            highscoreText.text = "HISCORE: " + highscore;

            // Save the new hiscore
            PlayerPrefs.SetInt("highscore", highscore);
        }
    }
}
