﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISenses : MonoBehaviour
{
	
	public float viewDistance = 10; 
	public float fieldOfView = 60; 
	public float hearingScale = 1.0f; 

	const float DEBUG_ANGLE_DISTANCE = 2.0f;
	const float DEGREES_TO_RADIANS = Mathf.PI / 180.0f;
	
	private Transform tf;

	
	void Start()
	{
		tf = GetComponent<Transform>();
	}

	
	void Update()
	{

	}

	public bool CanHear(GameObject target)
	{
		
		Noisemaker targetNoiseMaker = target.GetComponent<Noisemaker>();
		if (targetNoiseMaker == null)
		{
			return false;
		}

		
		Transform targetTf = target.GetComponent<Transform>();
		if (Vector3.Distance(targetTf.position, tf.position) <= targetNoiseMaker.volume * hearingScale)
		{
			return true;
		}

		
		return false;
	}

	public void DrawDebugAngle()
	{
		Vector3 perpendicularDirection = new Vector3(-tf.right.y, tf.right.x);
		float oppositeSideLength = Mathf.Tan(fieldOfView * 0.5f * DEGREES_TO_RADIANS) * DEBUG_ANGLE_DISTANCE;

		Debug.DrawLine(tf.position, tf.position + DEBUG_ANGLE_DISTANCE * tf.right + perpendicularDirection * oppositeSideLength, Color.green);
		Debug.DrawLine(tf.position, tf.position + DEBUG_ANGLE_DISTANCE * tf.right - perpendicularDirection * oppositeSideLength, Color.green);
	}

	public bool CanSee(GameObject target)
	{
			
		Transform targetTransform = target.GetComponent<Transform>();
		Vector3 vectorToTarget = targetTransform.position - tf.position;
		vectorToTarget.Normalize();

		DrawDebugAngle();

        if (Vector3.Angle(vectorToTarget, tf.right) >= fieldOfView)
        {
            return false;
        }

        RaycastHit2D hitInfo = Physics2D.Raycast(tf.position, vectorToTarget, viewDistance);
        Debug.DrawLine(tf.position, tf.position + vectorToTarget * viewDistance, Color.red);

        if (hitInfo.collider == null)
        {
            return false;
        }


        if (hitInfo.collider == target)
        {
            Debug.DrawLine(tf.position, tf.position + vectorToTarget * viewDistance, Color.red);
            return true;
        }


        return false;
	}
}
