﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
	public Transform tf;
	public Noisemaker noisemaker;

	public float moveSpeed = 5f;
	public float turnSpeed = 5;

	public float moveVolume = 5;
	public float turnVolume = 5;

	void Start () {
		tf = gameObject.transform;

		
		noisemaker = GetComponent<Noisemaker>();
	}
	
	void Update () {
		if (Input.GetKey(KeyCode.UpArrow))
		{
			Move(tf.right);
		}

		if (Input.GetKey(KeyCode.E))
		{
			Move(-tf.up);
		}

		if (Input.GetKey(KeyCode.DownArrow))
		{
			Move(-tf.right);
		}

		if (Input.GetKey(KeyCode.Q))
		{
			Move(tf.up);
		}

		if (Input.GetKey(KeyCode.LeftArrow))
		{
			Turn(true);
		}

		if (Input.GetKey(KeyCode.RightArrow))
		{
			Turn(false);
		}
	}

	public void Move(Vector3 direction)
	{
		// Move in the direction passed in, at speed "moveSpeed"
		tf.position += (direction.normalized * moveSpeed * Time.deltaTime);

		// noise
		if (noisemaker != null)
		{
			noisemaker.volume = Mathf.Max(noisemaker.volume, moveVolume);
		}
	}

	public void Turn(bool turn)
	{
		// Rotate based on turnSpeed and direction we are turning
		if (turn)
		{
			tf.Rotate(0, 0, turnSpeed);

			// noise
			if (noisemaker != null)
			{
				noisemaker.volume = Mathf.Max(noisemaker.volume, turnVolume);
			}
		}
		else
		{
			tf.Rotate(0, 0, -turnSpeed);
			
            // noise
			if (noisemaker != null)
			{
				noisemaker.volume = Mathf.Max(noisemaker.volume, turnVolume);
			}
		}
	}
}