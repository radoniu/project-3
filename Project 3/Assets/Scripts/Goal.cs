﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour {

    private GameController gameController;

    private void Start()
    {
        GameObject gameControllerObject =
    GameObject.FindWithTag("GameController");

        gameController =
            gameControllerObject.GetComponent<GameController>();
    }

    void OnCollisionEnter2D(Collision2D colision)
    {
        if (colision.gameObject.tag.Equals("Player"))
        {
            // Destroy coin
            Destroy(gameObject);
            // Add to the score
            gameController.IncrementScore();

            // Switch Scene
            SceneManager.LoadScene(3);
        }

    }

}