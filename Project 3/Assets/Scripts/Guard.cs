﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guard : MonoBehaviour
{

	private AISenses senses;



	public enum AIStates
	{
		Idle,
		Chase,
		LookAround,
		GoHome,
		
	}
	public Vector3 homePoint;
	public Vector3 goalPoint;
	public AIStates currentState;
	public float giveUpChaseDistance = 5f;
	public float closeEnough = 15f;

	public float moveSpeed = 5;
	public float turnSpeed = 15;
	Transform tf;

	// Use this for initialization
	void Start()
	{
		
		senses = GetComponent<AISenses>();

		tf = GetComponent<Transform>();

		
		homePoint = tf.position;
	}


	void Update()
	{
	
		switch (currentState)
		{
			case AIStates.Idle:
				
				Idle();
				
				if (senses.CanHear(GameController.instance.player.gameObject))
				{
					currentState = AIStates.LookAround;
				}
				if (senses.CanSee(GameController.instance.player.gameObject))
				{
					currentState = AIStates.Chase;
				}
				break;

			case AIStates.Chase:
				
				Chase();
				
				if (!senses.CanSee(GameController.instance.player.gameObject))
				{
					currentState = AIStates.LookAround;
				}
				if (Vector3.Distance(tf.position, GameController.instance.player.tf.position) > giveUpChaseDistance)
				{
					currentState = AIStates.GoHome;
				}
				break;

			case AIStates.LookAround:
				
				LookAround();
				
				if (senses.CanSee(GameController.instance.player.gameObject))
				{
					currentState = AIStates.Chase;
				}
				else if (Vector3.Distance(tf.position, GameController.instance.player.tf.position) > giveUpChaseDistance)
				{
					currentState = AIStates.GoHome;
				}
				else if (!senses.CanHear(GameController.instance.player.gameObject))
				{
					currentState = AIStates.GoHome;
				}
				break;

			case AIStates.GoHome:
				
				GoHome();
				
				if (senses.CanHear(GameController.instance.player.gameObject))
				{
					currentState = AIStates.LookAround;
				}
				if (senses.CanSee(GameController.instance.player.gameObject))
				{
					currentState = AIStates.Chase;
				}
				if (Vector3.Distance(tf.position, homePoint) <= closeEnough)
				{
					currentState = AIStates.Idle;
				}
				break;
		}
	}

	public void Idle()
	{
		// Do Nothing
	}

	public void Chase()
	{
		goalPoint = GameController.instance.player.tf.position;
		MoveTowards(goalPoint);
	}

	public void GoHome()
	{
		goalPoint = homePoint;
		MoveTowards(goalPoint);
	}

	public void LookAround()
	{
		Turn(true);
	}

	public void MoveTowards(Vector3 target)
	{
		if (Vector3.Distance(tf.position, target) > closeEnough)
		{
			
			Vector3 vectorToTarget = target - tf.position;
			tf.right = vectorToTarget;

			
			Move(tf.right);
		}
	}

	public void Move(Vector3 direction)
	{
		
		tf.position += (direction.normalized * moveSpeed*Time.deltaTime);
	}

	public void Turn(bool turn)
	{
	
		if (turn)
		{
			tf.Rotate(0, 0, turnSpeed * Time.deltaTime);
		}
		else
		{
			tf.Rotate(0, 0, -turnSpeed * Time.deltaTime);
		}
	}
}