﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour {
    public Transform patrolPoint;
   
    private Transform target;
   
    private Vector2 dir = new Vector2(1, 0);
    public float range;
    public float speed;
    public float turnSpeed;


    public float fieldOfView = 10f;
    public bool insight;
    public Vector2 hearing;

    Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
       
    }

    void Update()
    {
        float distance = Vector2.Distance(target.position, transform.position);
        if(distance <= fieldOfView)
        {
           
        }

        Debug.DrawRay(patrolPoint.position, dir * range);
        RaycastHit2D hit = Physics2D.Raycast(patrolPoint.position, dir, range);
        if(hit == true)
        {
            if (hit.collider.gameObject.tag.Equals("Border"))
            {
                Flip();
                speed *= -1;
                dir *= -1;
            }
        }
    }

    void FixedUpdate()
    {
            rb.velocity = new Vector2(speed, rb.velocity.y);
    }
    public void Flip()
    {
        transform.Rotate(0, 0, 180);
    }

}
